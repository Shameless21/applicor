package co.edu.uniajc.applicor.controller;

import co.edu.uniajc.applicor.model.ClienteModel;
import co.edu.uniajc.applicor.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cliente")
@Api("Licors")

public class ClienteController {

    private co.edu.uniajc.applicor.service.ClienteService ClienteService;

    @Autowired
    public ClienteController(ClienteService ClienteService) {
        this.ClienteService = ClienteService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = " Insert Cliente", response = ClienteModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Something Went Wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ClienteModel saveCliente(@RequestBody ClienteModel ClienteModel) {
        return ClienteService.createCliente(ClienteModel);
    }

    @PostMapping(path = "/update")
    @ApiOperation(value = "Update Cliente", response = ClienteModel.class)
    public ClienteModel updateCliente(@RequestBody ClienteModel ClienteModel) {
        return ClienteService.updateCliente(ClienteModel);
    }


    @GetMapping(path = "/all")
    @ApiOperation(value = "Find All Cliente", response = ClienteModel.class)
    public List<ClienteModel> findAllCliente() {
        return ClienteService.findAllCliente();
    }

    @GetMapping(path = "/all/name")
    @ApiOperation(value = "Find Cliente by name", response = ClienteModel.class)
    public List<ClienteModel> findAllClienteByname(@RequestParam(name = "name") String name) {
        return ClienteService.findAllClienteByname(name);
    }

}
