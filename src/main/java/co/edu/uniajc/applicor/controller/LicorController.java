package co.edu.uniajc.applicor.controller;
import co.edu.uniajc.applicor.model.LicorModel;
import co.edu.uniajc.applicor.service.LicorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Licor")
public class LicorController {
    private final LicorService licorService;

    @Autowired
    public LicorController(LicorService licorService) {
        this.licorService = licorService;
    }

    @PostMapping(path = "/save")
    public LicorModel saveLicor(@RequestBody LicorModel licorModel) {
        return licorService.createLicor(licorModel);
    }

    @PostMapping(path = "/update")
    public LicorModel updateLicor(@RequestBody LicorModel licorModel) {
        return licorService.updateLicor(licorModel);
    }


    @GetMapping(path = "/all")
    public List<LicorModel> findAllLicor() {
        return licorService.findAllLicor();
    }

    @GetMapping(path = "/all/name")
    public List<LicorModel> findAllLicorByName(@RequestParam(name = "name") String name) {
        return licorService.findAllLicorByName(name);
    }

    @GetMapping(path = "/all/price")
    public List<LicorModel> findAllLicorByPrice(@RequestParam(name = "price") Integer price) {
        return licorService.findAllPrice(price);
    }

}
