package co.edu.uniajc.applicor.controller;


import co.edu.uniajc.applicor.model.LicorModel;
import co.edu.uniajc.applicor.model.VendedorModel;
import co.edu.uniajc.applicor.service.VendedorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Vendedor")
@Api("Licors")

class VendedorController {

    private VendedorService VendedorService;

    @Autowired
    public VendedorController(VendedorService VendedorService) {
        this.VendedorService = VendedorService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = " Insert Vendedor", response = VendedorModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Something Went Wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public VendedorModel saveVendedor(@RequestBody VendedorModel VendedorModel) {
        return VendedorService.createVendedor(VendedorModel);
    }

    @PostMapping(path = "/update")
    @ApiOperation(value = "Update Vendedor", response = VendedorModel.class)
    public VendedorModel updateVendedor(@RequestBody VendedorModel VendedorModel) {
        return VendedorService.updateVendedor(VendedorModel);
    }


    @GetMapping(path = "/all")
    @ApiOperation(value = "Find All Vendedor", response = VendedorModel.class)
    public List<VendedorModel> findAllVendedor() {
        return VendedorService.findAllVendedor();
    }

    @GetMapping(path = "/all/name")
    @ApiOperation(value = "Find Vendedor by name", response = VendedorModel.class)
    public List<VendedorModel> findAllVendedorByname(@RequestParam(name = "name") String name) {
        return VendedorService.findAllVendedorByname(name);
    }

}
