package co.edu.uniajc.applicor.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.*;

@Entity
@Table(name = "cliente")

public class ClienteModel {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "correo")
    private String correo;

    @Column(name = "description")
    private String description;

    public  ClienteModel(){
        //Constructor
    }

    public ClienteModel(long id, String name, String direccion, String telefono, String correo, String description){
        this.id = id;
        this.name = name;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo = correo;
        this.description = description;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getdireccion() {
        return direccion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getcorreo() {
        return correo;
    }

    public void setcorreo(String name) {
        this.correo = correo;
    }


    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }

}
