package co.edu.uniajc.applicor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
)
public class ApplicorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicorApplication.class, args);
	}

}
