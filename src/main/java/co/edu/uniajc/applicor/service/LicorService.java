package co.edu.uniajc.applicor.service;

import co.edu.uniajc.applicor.model.LicorModel;
import co.edu.uniajc.applicor.repository.LicorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LicorService {
    private final LicorRepository licorRepository;

    @Autowired
    public LicorService(LicorRepository licorRepository){
        this.licorRepository = licorRepository;
    }

    public LicorModel createLicor (LicorModel licorModel){
        return licorRepository.save(licorModel);
    }

    public LicorModel updateLicor (LicorModel licorModel){
        return licorRepository.save(licorModel);
    }

    public List<LicorModel> findAllLicor (){
        return licorRepository.findAll();
    }

    public List<LicorModel> findAllLicorByName (String name){
        return licorRepository.findAllByNameContains(name);
    }

    public List<LicorModel> findAllPrice (Integer price){
        return licorRepository.findPrice(price);
    }


}
