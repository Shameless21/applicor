package co.edu.uniajc.applicor.service;

import co.edu.uniajc.applicor.model.ClienteModel;
import co.edu.uniajc.applicor.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service

public class ClienteService {


    private final ClienteRepository ClienteRepository;

    @Autowired
    public ClienteService(ClienteRepository ClienteRepository) {
        this.ClienteRepository = ClienteRepository;
    }

    public ClienteModel createCliente(ClienteModel ClienteModel) {
        return ClienteRepository.save(ClienteModel);
    }

    public ClienteModel updateCliente(ClienteModel ClienteModel) {
        return ClienteRepository.save(ClienteModel);
    }

    public List<ClienteModel> findAllCliente() {
        return ClienteRepository.findAll();
    }

    public List<ClienteModel> findAllClienteByname(String name) {
        return ClienteRepository.findAllByNameContains(name);
    }

}
