package co.edu.uniajc.applicor.service;


import co.edu.uniajc.applicor.model.LicorModel;
import co.edu.uniajc.applicor.model.VendedorModel;
import co.edu.uniajc.applicor.repository.LicorRepository;
import co.edu.uniajc.applicor.repository.VendedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class VendedorService {

    private final VendedorRepository VendedorRepository;

    @Autowired
    public VendedorService(VendedorRepository VendedorRepository) {
        this.VendedorRepository = VendedorRepository;
    }

    public VendedorModel createVendedor(VendedorModel VendedorModel) {
        return VendedorRepository.save(VendedorModel);
    }

    public VendedorModel updateVendedor(VendedorModel VendedorModel) {
        return VendedorRepository.save(VendedorModel);
    }


    public List<VendedorModel> findAllVendedor() {
        return VendedorRepository.findAll();
    }

    public List<VendedorModel> findAllVendedorByname(String name) {
        return VendedorRepository.findAllByNameContains(name);
    }

}