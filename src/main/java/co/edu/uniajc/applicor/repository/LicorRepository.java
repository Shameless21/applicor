package co.edu.uniajc.applicor.repository;

import co.edu.uniajc.applicor.model.LicorModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface LicorRepository extends JpaRepository<LicorModel,Integer> {


    List<LicorModel> findAllByNameContains(String name);
    LicorModel getById(Long id);

    @Query(nativeQuery = true, value = "SELECT" +
            "lc_id, lc_name, " +
            "lc_description, " +
            "lc_price" +
            "FROM licor"+
            "WHERE lc_price=: price")
    List<LicorModel> findPrice (@Param(value = "price") Integer price);
}
