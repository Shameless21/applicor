package co.edu.uniajc.applicor.repository;


import co.edu.uniajc.applicor.model.LicorModel;
import co.edu.uniajc.applicor.model.VendedorModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface VendedorRepository extends JpaRepository<VendedorModel,String> {

    List<VendedorModel> findAllByNameContains(String name);
    VendedorModel getById(Long id);


}
