package co.edu.uniajc.applicor.repository;


import co.edu.uniajc.applicor.model.ClienteModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ClienteRepository extends JpaRepository<ClienteModel,String> {

    List<ClienteModel> findAllByNameContains(String name);
    ClienteModel getById(Long id);

}
