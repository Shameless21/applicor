CREATE TABLE "licor" (

          id serial not null,
          name character varying(50) not null,
          description character varying(100) not null,
          price Integer not null
);

ALTER TABLE "licor" OWNER to postgres;

CREATE TABLE "cliente" (

          id serial not null,
          name character varying(50) not null,
          direccion character varying(50) not null,
          telefono character varying(50) not null,
          email character varying(50) not null,
          description character varying(100) not null

);
ALTER TABLE "cliente" OWNER to postgres;



CREATE TABLE "vendedor" (

          id serial not null,
          name character varying(50) not null,
          direccion character varying(50) not null,
          telefono character varying(50) not null,
          email character varying(50) not null,
          description character varying(100) not null

);
ALTER TABLE "vendedor" OWNER to postgres;
